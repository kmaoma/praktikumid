package praktikum3;

import lib.TextIO;

public class CumLaude {

	public static void main(String[] args) {
		
		//töö 2:küsida uut sisestust, mitte lõpetada töö vigase sisendi korral
		
		while (true) {
			System.out.println("Palun sisesta keskmine hinne");
			keskminehinne = TextIO.getlnDouble();
			if (keskminehinne < 0 || keskminehinne > 5) {
				System.out.println("Vigane hinne proovi uuesti");
			} else {
				break;
			}
			
		}
		
		System.out.println("Palun sisesta lõputöö hinne");
		int l6put66 = TextIO.getlnInt();
		
		if (l6put66 <0 || l6put66 > 5 ) {
			System.out.println("Vigane hinne!");
			return;
			
		}
		
		if (keskminehinne > 4.5) {
			if (l6put66 == 5) {
				// või kirjutad if (keskminehinne > 4.5 && 5 == l6put66) {
			System.out.println("Jah, saad Cum Laude");
			return;
			
		}
			
	}
    System.out.println("Ei saa");
}
}
