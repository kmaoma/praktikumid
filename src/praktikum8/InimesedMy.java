package praktikum8;

import java.util.ArrayList;

import lib.TextIO;

public class InimesedMy {

	public static void main(String[] args) {

		ArrayList<Inimene> inimesed = new ArrayList<Inimene>();

		while (true) {
			System.out.println("Palun sisesta nimi ja vanus");
			String nimi = TextIO.getlnString();
			if (nimi.equals(""))
				break;
			int vanus = TextIO.getlnInt();
			Inimene keegi = new Inimene(nimi, vanus);
			inimesed.add(keegi);

		}

		for (Inimene inimene : inimesed) {
			// Java kutsub välja Inimene klassi toString() meetodi
			inimene.tervita();
		}

	}

}

class Inimene {
	
	String nimi;
	int vanus;
	
	public Inimene(String nimi, int vanus) {
		this.nimi = nimi;
		this.vanus = vanus;
	}
	
	public boolean equals(Inimene teine) {
		return teine.vanus == this.vanus
				&& this.nimi.equals(teine.nimi);
	}
	
	public void tervita() {
		TextIO.putln("Tere, minu nimi on " + nimi + ", olen " + vanus + "-aastane.");
	}
	
	@Override
	public String toString() {
		return nimi + " " + vanus;
	}
}
