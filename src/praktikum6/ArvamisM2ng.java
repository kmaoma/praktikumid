package praktikum6;

import lib.TextIO;

public class ArvamisM2ng {

	public static void main(String[] args) {

        int arvutiArv = suvalineArv(1, 100);
        
        while (true) {
            System.out.println("Arva ära, mis number on? ");
            int kasutajaArv = TextIO.getlnInt();
            if (arvutiArv == kasutajaArv) {
                System.out.println("Arvasid ära! Jess!");
                break;
            } else if (arvutiArv > kasutajaArv) {
                System.out.println("Vale, see arv on suurem");
            } else {
                System.out.println("Vale, see arv on väiksem");
            }
        }
    }

    public static int suvalineArv(int min, int max) {
        int vahemik = max - min + 1;
        return (int) (Math.random() * vahemik) + min;
    }
}
