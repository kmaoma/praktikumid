package praktikum5;

import lib.TextIO;

public class Tunnitoo2 {

	public static void main(String[] args) {
		
		int kasutajaSisestus = kasutajaSisestus(1, 10);
		System.out.println("kasutajaSisestus meetod tagastas: " + kasutajaSisestus);
		
	}
	
	 public static int kasutajaSisestus(int min, int max) {
	        while (true) {
	            System.out.println("Palun sisesta number vahemikus " + min + " kuni " + max);
	            int sisestus = TextIO.getlnInt();
	            if (sisestus >= min && sisestus <= max) {
	                return sisestus;
	            } else {
	                System.out.println("Vigane sisestus! Proovi uuesti.");
	            }
	        }
	    }
	    
}
//variant 2
//public static int kasutajaSisestus(int min, int max) {

//int sisestus;
//do {
   // System.out.println("Palun sisesta number vahemikus " + min + " kuni " + max);
   // sisestus = TextIO.getlnInt();           
//} while (sisestus < min || sisestus > max);
//return sisestus;
//}