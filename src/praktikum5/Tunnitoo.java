package praktikum5;

import lib.TextIO;

public class Tunnitoo {

	public static void main(String[] args) {
		
		System.out.println("Palun sisesta arv");
		int arv = TextIO.getlnInt();
		int arvKuubis = kuup(arv);
		System.out.println(arvKuubis);
		
		
	}

	public static int kuup(int sisendv22rtus) {
		 int tagastusv22rtus = (int) Math.pow(sisendv22rtus, 3);
		 return tagastusv22rtus;
	}
}
