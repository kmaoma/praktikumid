package praktikum2;

import lib.TextIO;

public class GrupiSuurus {
	
	public static void main(String[] args) {
		
		System.out.println("Palun sisesta inimeste arv");
		int inimesteArv = TextIO.getlnInt();
		
		System.out.println("Sisesta grupi suurus");
		int grupiSuurus = TextIO.getlnInt();
		
		int gruppideArv = (int) Math.floor(inimesteArv / grupiSuurus);
		System.out.println("saab moodustada " + gruppideArv + " gruppi");
		
		int j22k = inimesteArv % grupiSuurus;
		System.out.println("j22k on: " + j22k);

	}
}
