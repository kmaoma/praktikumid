package praktikum4;

public class Hrj5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//Trükkida ekraanile selline tabel koos ääristega. Tabeli kõrgus on sama mis laius - see suurus küsi kasutaja käest. 
        
        int tabeliSuurus = 7;

        for (int i = 0; i < tabeliSuurus * 2 + 3; i++) {
            System.out.print("-");  
        }
        System.out.println();
        
        
        for (int i = 0; i < tabeliSuurus; i++) {
            System.out.print("| ");
            for (int j = 0; j < tabeliSuurus; j++) {
                if (j == i || i == 2) {
                    System.out.print("x ");
                } else {
                    System.out.print("0 ");
                }
                //System.out.print("(i=" + i + "j=" + j + ")");
            }
            System.out.println("|");
        }
        
        
        for (int i = 0; i < tabeliSuurus * 2 + 3; i++) {
            System.out.print("-");  
        }
        System.out.println();
    }

}
