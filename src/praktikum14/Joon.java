package praktikum14;

public class Joon {

	Punkt algusPunkt, l6ppPunkt;

	public Joon(Punkt p1, Punkt p2) {
		algusPunkt = p1;
		l6ppPunkt = p2;

	}

	@Override
	public String toString() {

		return "Joon(" + algusPunkt + ", " + l6ppPunkt + ")";
	}

	public double pikkus() {
		double a = l6ppPunkt.y - algusPunkt.y;
		double b = l6ppPunkt.x - algusPunkt.x;
		double h = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
		return h;

	}
}