package praktikum14;

public class Ring {

	Punkt keskpunkt;
	double raadius;

	public Ring(Punkt p, double r) {
		this.keskpunkt = p;
		this.raadius = r;
	}
	
	public double ymberm66t(){
		return 2 * Math.PI * raadius;
	}
	
	private double pindala() {
		return Math.PI * Math.pow(raadius, 2);
	}
	
	@Override
	public String  toString() {
		return "Ring, ümbermõõt on: " + ymberm66t()
			+ ", pindala on: " + pindala(); 
		
		
		
		

	}

}
