package praktikum14;

public class KatsetusPunkt {

	public static void main(String[] args) {

		Punkt minuPunkt = new Punkt();
		minuPunkt.x = 100;
		minuPunkt.y = 200;

		Punkt veelYksPunkt = new Punkt(300, 400);

		System.out.println(minuPunkt);
		System.out.println(veelYksPunkt);
		
		Joon minuJoon = new Joon(minuPunkt, veelYksPunkt);
		System.out.println(minuJoon);
		System.out.println("Joone pikkus on: " + minuJoon.pikkus());
		
		Ring minuRing = new Ring(minuPunkt, 50.);
		System.out.println(minuRing);
		
		Silinder minuSilinder = new Silinder(minuRing, 34.);
		System.out.println(minuSilinder);

	}

}
