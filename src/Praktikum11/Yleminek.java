package Praktikum11;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class Yleminek extends Applet {

	@Override
	public void paint(Graphics g) {

		int w = getWidth();
		int h = getHeight();

		double v2rviMuutus = 255.0 / h;
		System.out.println(v2rviMuutus);

		for (int i = 0; i < h; i++) {
			int v2rviKood = (int) (255 - v2rviMuutus * i);
			Color minuV2rv = new Color(v2rviKood, v2rviKood, v2rviKood);
			new Color(v2rviKood, v2rviKood, v2rviKood);

			g.setColor(minuV2rv);
			g.drawLine(0, i, w, i);

		}

	}
}
