package Praktikum10;

import java.io.File;
import java.util.Arrays;

public class FailiNimekiri {

    public static void trykiFailid(String kataloogiTee) {

        File kataloog = new File(kataloogiTee);
        File[] failid = kataloog.listFiles();
        Arrays.sort(failid);

        for (File file : failid) {
            if (file.isDirectory()) {
                System.out.print("Kataloog: " + file.getName());
                //trykkida kataloogi sisu
                //trykiFailid meetodit kasutades
            trykiFailid(file.getAbsolutePath());
            }
            else {
                System.out.print("Fail:     ");
                System.out.println(file.getAbsoluteFile());
            }
           
        }
    }

    public static void main(String[] args) {
        trykiFailid("/home/kmaoma/Documents/workspace/Praktikumid/src");
    }
}
