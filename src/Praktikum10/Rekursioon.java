package Praktikum10;

public class Rekursioon {
	
	public static void main(String[] args) {
	
		int i = 0;
		while (true) {
			System.out.println(i + " - " + fibonacci(i));
			i++;
			
		}
	}
	
	
	public static long fibonacci(int n) {
		if (n == 0)
			return 0;
		else if (n == 1)
			return 1;
		else
		return fibonacci(n - 1) + fibonacci(n - 2);
		
	
}
	
		public static int astenda(int arv, int aste){
			if (aste == 1)
				return arv;
			else
			return arv * astenda(arv, aste - 1);

	}

}
