package praktikum9;

public class MassiiviSuurus {

	public static void main(String[] args) {
		
		int[] massiiv = {1, 3, 6, 7, 8, 3, 5, 7, 21, 3}; 
		System.out.println(suurim(massiiv));
	}
	
	public static int suurim(int[] sisend){
		
		int seniSuurim = 0;
		for (int i : sisend) {
			if (i > seniSuurim)
				seniSuurim = i;
		}
	
	
		return sisend[0];

	}

}
