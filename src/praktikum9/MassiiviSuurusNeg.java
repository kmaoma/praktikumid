package praktikum9;

public class MassiiviSuurusNeg{

	public static void main(String[] args) {
		
		
		int[] massiiv = {-1, -3, -6}; 
		System.out.println(suurim(massiiv));
	}
	
	public static int suurim(int[] sisend){
		
		int seniSuurim = sisend[0];
		for (int i : sisend) {
			if (i > seniSuurim)
				seniSuurim = i;
		}
	
	
		return sisend[0];

	}

}